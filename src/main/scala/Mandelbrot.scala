import processing.core.PApplet
import processing.core.PConstants
import processing.event

class Mandelbrot extends PApplet {

    private class Point(val x: Double, val y: Double) {
        def this(p: Point) = this(p.x, p.y)

        def squared(): Point        = new Point(x * x,   y * y)
        def scale(s: Double): Point = new Point(x * s,   y * s)
        def adjust(q: Point): Point = new Point(x + q.x, y + q.y)
    }

    private var graphLimitsLower = new Point(-2.5d, -1.2d)
    private var graphLimitsUpper = new Point( 1.0d,  1.2d)

    private val screenUnit: Int = 200
    private val windowX: Int    = screenUnit * (graphLimitsLower.x.abs + graphLimitsUpper.x.abs).toInt
    private val windowY: Int    = screenUnit * (graphLimitsLower.y.abs + graphLimitsUpper.y.abs).toInt

    private val scaleAmount = 1.1d
    private var offset   = new Point(0.0d, 0.0d)
    private var startPan = new Point(0.0d, 0.0d)


    override def settings(): Unit = {
        size(windowX, windowY)
        pixelDensity(1)
    }

    override def setup(): Unit = {
        colorMode(PConstants.HSB)
    }

    override def keyPressed(): Unit = {
        key match {
            case 'i' => zoomOnMouse(-1)
            case 'o' => zoomOnMouse( 1)
            case _ =>
        }
    }

    override def mouseWheel(e: event.MouseEvent): Unit = {
        zoomOnMouse(e.getCount)
    }

    private def zoomOnMouse(scrollCount: Int): Unit = {
        if (scrollCount == 0) { return }

        val mousePreZoom = windowToScreen(mouseX, mouseY)

        val scale = if (scrollCount > 0) scaleAmount else 1 / scaleAmount

        graphLimitsLower = graphLimitsLower.scale(scale)
        graphLimitsUpper = graphLimitsUpper.scale(scale)

        val mousePostZoom = windowToScreen(mouseX, mouseY)

        val dMouse = new Point(
            mousePreZoom.x - mousePostZoom.x,
            mousePreZoom.y - mousePostZoom.y
        )

        graphLimitsLower = graphLimitsLower.adjust(dMouse)
        graphLimitsUpper = graphLimitsUpper.adjust(dMouse)
    }

    override def mousePressed(): Unit = {
        startPan = new Point(mouseX, mouseY)
    }

    override def mouseDragged(): Unit = {
        offset = new Point(
            offset.x - (mouseX - startPan.x),
            offset.y - (mouseY - startPan.y)
        )

        startPan = new Point(mouseX, mouseY)
    }

    override def draw(): Unit = {
        loadPixels()
        drawMandelbrot()
        updatePixels()
    }

    private def drawMandelbrot(): Unit = {
        val maxIterations = 100
        pixels.zipWithIndex.collect {
            case(_, i) =>
                val iteration = determineIterationCount(new Point(i % windowX, i / windowX), maxIterations)
                setPixelColour(iteration, maxIterations, i)
        }
    }

    private def determineIterationCount(pixel: Point, max: Int): Int = {
        val p0 = windowToScreen(pixel.x, pixel.y)
        var p  = new Point(0, 0)

        var iteration = 0

        while (notDiverging(p) && iteration < max) {
            p = nextPoint(p, p0)
            iteration += 1
        }

        iteration
    }

    private def windowToScreen(x: Double, y: Double): Point = {
        new Point(
            mapToRange(x + offset.x, windowX, graphLimitsLower.x, graphLimitsUpper.x),
            mapToRange(y + offset.y, windowY, graphLimitsUpper.y, graphLimitsLower.y)
        )
    }

    // Assumes the input range always starts at 0
    private def mapToRange(v: Double, e1: Double, s2: Double, e2: Double): Double = {
        s2 + ((e2 - s2) * (v / e1))
    }

    private def notDiverging(p: Point): Boolean = {
        val divergenceLimit = 4
        p.squared().x + p.squared().y <= divergenceLimit
    }

    private def nextPoint(p: Point, p0: Point): Point = {
        new Point(
            p.squared().x - p.squared().y + p0.x,
            (2 * p.x * p.y) + p0.y
        )
    }

    private def setPixelColour(n: Int, max: Double, p: Int): Unit = {
        pixels(p) = color(255 * (n / max).toFloat, 255, 255)
    }
}

object Mandelbrot {
    def main(args: Array[String]): Unit = {
        PApplet.main("Mandelbrot")
    }
}